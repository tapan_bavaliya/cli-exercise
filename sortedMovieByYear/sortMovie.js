const readfile = require('../readfile');

const movie = (year) => {
  return readfile().then((parsedMovies) => {

    // Make listOfMovies that have same release date as year
    const listOfMovies = parsedMovies.filter((parsedMovie)=>{
      const years = parsedMovie.release_date.split('-')[0];
      if (years === year){
        return parsedMovie;
      }
      return false; // Already return above
    });

    // Sort That movie object by decsending order
    const listOfMoviesSortedByVoteAverage = listOfMovies.sort((first, second)=>{
      return second.vote_average - first.vote_average;
    });

    // Return that sorted movie by title
    const movies = listOfMoviesSortedByVoteAverage.map((parsedMovie)=>{
      return {
        originalTitle: parsedMovie.original_title,
        title: parsedMovie.title,
        voteAverage: parsedMovie.vote_average,
        revenue: parsedMovie.revenue,
        releaseDate: parsedMovie.release_date,
        voteCount: parsedMovie.vote_count,
      };
    });

    return movies;
  });
};
module.exports = movie;
