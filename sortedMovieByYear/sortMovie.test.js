const sortMovie = require('./sortMovie');

const movies = [{
  originalTitle: 'Intolerance',
  releaseDate: '1916-09-04',
  revenue: '8394751',
  title: 'Intolerance',
  voteAverage: '7.4',
  voteCount: '60',
}];

const year = '1916';

test(`List Movies by sorting vote-average & display that movie name`, ()=>{
  // eslint-disable-next-line no-console
  console.log('===================');
  // eslint-disable-next-line no-console
  console.log('Input => year = 1916');
  // eslint-disable-next-line no-console
  console.log('===================');
  // eslint-disable-next-line no-console
  console.log('===================');
  // eslint-disable-next-line no-console
  console.log(`{'originalTitle': 'Intolerance',
              'releaseDate': '1916-09-04',
              'revenue': '8394751',
              'title': 'Intolerance',
              'voteAverage': '7.4',
              'voteCount': '60'}`);
  // eslint-disable-next-line no-console
  console.log('===================');
  return expect(sortMovie(year)).resolves.toEqual(movies);
});
