const sortMovie = require('./sortMovie');
const Table = require('cli-table');

const table = new Table({
  head: ['Title', 'Original Title', 'Revenue', 'Vote Average', 'Release Date', 'Vote Count'],
  colWidths: [40, 40, 13, 5, 15, 8],
});

const year = process.argv[2];

if (!year){
  // eslint-disable-next-line no-console
  console.log(`Please enter year after command..!!`);
  // eslint-disable-next-line consistent-return
  return false;
}

// Print in table view
sortMovie(year).then((movies)=>{
  movies.forEach((movie)=>{
    table.push([
      [movie.title],
      [movie.originalTitle],
      [movie.revenue],
      [movie.voteAverage],
      [movie.releaseDate],
      [movie.voteCount],
    ]);
    return false;
  });
}).then(()=>{
  // eslint-disable-next-line no-console
  console.log(table.toString());
});
