# CLI EXERCISE

For this exercise we have movies dataset, on which you have to perfrom some task, and get the desire output
  
Data set we are using is from from this url
https://www.kaggle.com/tmdb/tmdb-movie-metadata/data

## SETUP

- Git Clone
```
$ git clone https://gitlab.com/tapan_bavaliya/cli-exercise
```

- Install npm
```
$ npm install
```


## TASK

### Question - 1) Display the list of movies based one year passed as argument & movies is sorted based on rating

#### Answer:

- Run
```
$ node sortedMovieByYear/sortPrinter.js {year}
```

- Run Test Case
```
$ npm test
```

![sortedMovieByYear](./images/sortedMovieByYear.png)

### Question - 2) Display the list of movies which have highest revenue of categories(genres)

fields in must be
* title
* original_title
* release_date
* budget
* original_language
* revenue
* vote_average

for example 

| categories | title | original_title | release_date | budget | original_language | revenue | vote_average |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| Action | Avatar | Avatar | 2009-12-10 | 237000000 | en | 2787965087 | 7.2 |
| Crime | Spectre  | Spectre  | 2015-10-26 | 245000000 | en | 880674609 | 6.3 |

and so on

#### Answer:

- Run
```
$ node highestMovieCategory/mainPrinter.js
```

- Run Test Case
```
$ npm test
```

![highestMovieCategory](./images/highestMovieCategory.png)

### Question - 3) list the following field which is populated from the dataset

fields
* yearly total budget - addition of budgets of movies produced in the year 
* yearly revenue - addition of revenue of movies produced in the year
* yearly profilt - net profile of movies produced in the year
* highest profitable category of year - show the category which get highest profilt
* highest categories movies name - movie name which get highest profilt from that categories
* profitable movie budget
* profitable movie ravenue
* percentage share in profit of movies which get highest profilt

| budget - year |  revenue - year | profilt - year | highest profitable category - year | highest categories movies name - year | profitable movie budget | profitable movie ravenue | percentage share in profitable movies |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 

#### Answer:

- Run
```
$ node sortedMovieByHighestCategory/sortCategoryPrinter.js
```

- Run Test Case
```
$ npm test
```

![sortedMovieByHighestCategory](./images/sortedMovieByHighestCategory.png)