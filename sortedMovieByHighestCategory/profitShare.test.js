const profitShare = require('./profitShare');


const highestMovieByProfit = {
  Budget: '245000',
  // eslint-disable-next-line max-len
  genres: '[{"id": 18, "name": "Drama"}, {"id": 10749, "name": "Romance"}, {"id": 10752, "name": "War"}]',
  id: '3060',
  original_language: 'en',
  original_title: 'The Big Parade',
  release_date: '1925-11-05',
  revenue: '22000000',
  title: 'The Big Parade',
  vote_average: '7.0',
  vote_count: '21',
};

const profitCategory = 21755000;

test('highest Category', ()=>{
  return expect(profitShare(highestMovieByProfit, profitCategory)).toEqual(100);
});
