const findCategoryName = (movies)=>{
  let categoriesList = [];
  movies.forEach((movie)=>{
    const categories = JSON.parse(movie.genres);
    categories.forEach((category)=>{
      categoriesList.push(category.name);
      return false;
    });
    return false;
  });
  return categoriesList;
};
module.exports = findCategoryName;
