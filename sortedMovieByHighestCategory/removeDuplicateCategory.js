const removeDuplicateCategory = (categories) =>{
  const removeDuplicate = categories.filter((item, pos) => {
    return categories.indexOf(item) === pos;
  });
  return removeDuplicate;
};
module.exports = removeDuplicateCategory;
