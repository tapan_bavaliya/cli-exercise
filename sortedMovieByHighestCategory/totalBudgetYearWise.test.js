const totalBudgetYearWise = require('./totalBudgetYearWise');

const movies = require('../data/movies');

test('Total Budget', ()=>{
  return expect(totalBudgetYearWise(movies)).toEqual(537000000);
});
