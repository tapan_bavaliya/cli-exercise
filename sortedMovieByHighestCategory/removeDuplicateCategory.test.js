const removeDuplicateCategory = require('./removeDuplicateCategory');

const movieCategory = ['Action',
  'Adventure',
  'Fantasy',
  'Science Fiction',
  'Adventure',
  'Fantasy',
  'Family',
  'Science Fiction'];

const output = ['Action', 'Adventure', 'Fantasy', 'Science Fiction', 'Family'];

test('Remove Duplicate', ()=>{
  return expect(removeDuplicateCategory(movieCategory)).toEqual(output);
});
