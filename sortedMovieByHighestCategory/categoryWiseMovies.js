const moviesCategoryWise = require('./moviesCategoryWise');

const categoryWiseMovies = (categories, moviesWithSameYear) =>{
  const categoryWiseMoviesObject = categories.map((category)=>{
    return {
      CategoryName: category,
      CategoryMovies: moviesCategoryWise(moviesWithSameYear, category),
    };
  });
  return categoryWiseMoviesObject;
};
module.exports = categoryWiseMovies;
