const totalRevenueYearWise = require('./totalRevenueYearWise');

const movies = require('../data/movies');

test('Total Revenue', ()=>{
  return expect(totalRevenueYearWise(movies)).toEqual(3748965087);
});
