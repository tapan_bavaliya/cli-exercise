const totalBudgetYearWise = (movies) =>{
  let totalBudget = 0;
  movies.forEach((movie)=>{
    totalBudget += parseInt(movie.Budget, 0);
    return false;
  });
  return totalBudget;
};
module.exports = totalBudgetYearWise;
