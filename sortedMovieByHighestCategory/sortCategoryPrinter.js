const sortCategory = require('./sortCategory');
const Table = require('cli-table');

const table = new Table({
  head: ['year',
    'budget - year',
    'revenue - year',
    'profilt - year',
    'highest profitable category - year',
    'highest categories movies name - year',
    'profitable movie budget',
    'profitable movie revenue',
    'profitable movie profit',
    'percentage share in profitable movies'],
  colWidths: [6, 13, 13, 13, 40, 40, 25, 25, 30, 20],
});

sortCategory().then((movies)=>{
  movies.forEach((movie)=>{
    // console.log(movie)
    table.push([
      [movie.year],
      [movie.totalbudget],
      [movie.totalRevenue],
      [movie.totalProfit],
      movie.category,
      [movie.title],
      [movie.Budget],
      [movie.revenue],
      [movie.profit],
      [movie.share],
    ]);
    return false;
  });
}).then(()=>{
// eslint-disable-next-line no-console
  console.log(table.toString());
});
