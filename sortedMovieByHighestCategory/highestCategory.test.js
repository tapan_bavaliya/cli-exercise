const highestCategory = require('./highestCategory');

const year = '2009';
const movies = require('../data/movies');

const output =
  {
    year: '2009',
    totalbudget: 237000000,
    totalRevenue: 2787965087,
    totalProfit: 2550965087,
    category: 'Action',
    title: 'Avatar',
    Budget: '237000000',
    revenue: '2787965087',
    profit: 2550965087,
    share: 100,
  };

test('highest Category', ()=>{
  return expect(highestCategory(year, movies)).toEqual(output);
});
