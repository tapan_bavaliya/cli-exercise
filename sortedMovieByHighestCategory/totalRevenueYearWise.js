const totalRevenueYearWise = (movies) =>{
  let totalRevenue = 0;
  movies.forEach((movie)=>{
    totalRevenue += parseInt(movie.revenue, 0);
    return false;
  });
  return totalRevenue;
};
module.exports = totalRevenueYearWise;
