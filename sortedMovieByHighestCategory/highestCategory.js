const totalBudgetYearWise = require('./totalBudgetYearWise');
const totalRevenueYearWise = require('./totalRevenueYearWise');
const totalProfitYearWise = require('./totalProfitYearWise');
const findCategoryName = require('./findCategoryName');
const removeDuplicateCategory = require('./removeDuplicateCategory');
const categoryWiseMovies = require('./categoryWiseMovies');
const categoryWiseMoviesWithProfit = require('./categoryWiseMoviesWithProfit');
const profitShare = require('./profitShare');

const highestCategory = (year, movies) =>{

  let moviesWithSameYear = [];
  // split year from release_date
  moviesWithSameYear = movies.filter((movie) =>{
    let years = movie.release_date.split('-')[0];
    return years === year;
  });

  // Find total Budget
  const budget = totalBudgetYearWise(moviesWithSameYear);

  // Find total Revenue
  const revenue = totalRevenueYearWise(moviesWithSameYear);

  // Find total Profit
  const profit = totalProfitYearWise(budget, revenue);

  // Find Category of that Movie object
  const categories = findCategoryName(moviesWithSameYear);

  // Remove Duplicate in that
  const removeDuplicateCategories = removeDuplicateCategory(categories);
  const categoryWiseMoviesObjects = categoryWiseMovies(removeDuplicateCategories, moviesWithSameYear);

  // eslint-disable-next-line max-len
  const categoryWiseMoviesWithProfitObjects = categoryWiseMoviesWithProfit(categoryWiseMoviesObjects);

  // Each element in array find that movies that match all condition given in question
  // eslint-disable-next-line max-len
  const categoryWiseMoviesWithProfitSorted = categoryWiseMoviesWithProfitObjects.sort((first, second)=>{
    return second.profit - first.profit;
  });

  const categoryIndex = categoryWiseMoviesObjects.map((movie)=>{
    return movie.CategoryName;
  }).indexOf(categoryWiseMoviesWithProfitSorted[0].CategoryName);

  const sortedProfitWiseMovie = categoryWiseMoviesObjects[categoryIndex].CategoryMovies;

  const sortedMovie = sortedProfitWiseMovie.sort((first, second)=>{
    return ((second.revenue - second.Budget) - (first.revenue - first.Budget));
  });

  const highestMovieByProfit = sortedMovie[0];

  let categoryTotalProfit = profit === 0 ? 1 : profit;

  return {
    year,
    totalbudget: budget,
    totalRevenue: revenue,
    totalProfit: profit,
    category: categoryWiseMoviesWithProfitSorted[0].CategoryName,
    title: highestMovieByProfit.title,
    Budget: highestMovieByProfit.Budget,
    revenue: highestMovieByProfit.revenue,
    profit: (highestMovieByProfit.revenue - highestMovieByProfit.Budget),
    share: profitShare(highestMovieByProfit, categoryTotalProfit),
  };

};
module.exports = highestCategory;
