const categoryWiseMoviesWithProfit = require('./categoryWiseMoviesWithProfit');

const categoryWiseMovieList =
 [{ CategoryName: 'Drama', CategoryMovies: [[Object]] },
   { CategoryName: 'Romance', CategoryMovies: [[Object]] },
   { CategoryName: 'War', CategoryMovies: [[Object]] },
   { CategoryName: 'Comedy', CategoryMovies: [[Object]] },
   { CategoryName: 'Music', CategoryMovies: [[Object]] }];

const output =
      [{ CategoryName: 'Drama', profit: NaN },
        { CategoryName: 'Romance', profit: NaN },
        { CategoryName: 'War', profit: NaN },
        { CategoryName: 'Comedy', profit: NaN },
        { CategoryName: 'Music', profit: NaN }];

test('highest Category', ()=>{
  return expect(categoryWiseMoviesWithProfit(categoryWiseMovieList)).toEqual(output);
});
