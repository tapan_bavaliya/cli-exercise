const moviesCategoryWise = (moviesWithSameYear, allCategory) =>{
  let movies = [];

  moviesWithSameYear.forEach((movie)=>{
    const categories = JSON.parse(movie.genres);
    categories.forEach((category) =>{
      if (category.name === allCategory){
        movies.push(movie);
      }
      return false;
    });
    return false;
  });
  return movies;
};
module.exports = moviesCategoryWise;
