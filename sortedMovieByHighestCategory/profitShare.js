const profitShare = (highestMovieByProfit, categoryTotalProfit) =>{
  let share = (highestMovieByProfit.revenue - highestMovieByProfit.Budget) * 100;
  share /= categoryTotalProfit;
  return share;
};
module.exports = profitShare;
