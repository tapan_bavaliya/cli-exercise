const readfile = require('../readfile');

const highestCategory = require('./highestCategory');


let highestMovieCategorywise = () =>{
  return readfile().then((movies)=>{
    // Find all year
    let years = movies.map((movie)=>{
      return movie.release_date.split('-')[0];
    });

    // Remove Same in that array
    years = years.filter((item, pos) => {
      return years.indexOf(item) === pos;
    });

    const nullYear = years.indexOf('');
    years.splice(nullYear, 1);

    // Get one year & Find Highest in that
    const movieObject = years.map((moviesYear)=>{
      return highestCategory(moviesYear, movies);
    });

    const moviesSortedByYear = movieObject.sort((first, second)=>{
      return second.year - first.year;
    });

    return moviesSortedByYear;
  }).catch((error)=>{
    // eslint-disable-next-line no-console
    console.log(error);
  });
};
module.exports = highestMovieCategorywise;
