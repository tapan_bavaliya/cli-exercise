const totalProfitCategoryWise = (movies) =>{
  let totalProfit = 0;
  movies.forEach((movie)=>{
    totalProfit += movie.revenue - movie.Budget;
    return false;
  });
  return totalProfit;
};
module.exports = totalProfitCategoryWise;
