const findCategoryName = require('./findCategoryName');

const movies = require('../data/movies');

// eslint-disable-next-line max-len
const output = ['Action', 'Adventure', 'Fantasy', 'Science Fiction', 'Adventure', 'Fantasy', 'Action'];

test('test for find category name', ()=>{
  expect(findCategoryName(movies)).toEqual(output);
});
