const highestMovieCategorywise = require('./sortCategory');


const output =
  {
    Budget: '0',
    category: 'Comedy',
    profit: 0,
    revenue: '0',
    share: 0,
    title: 'Growing Up Smith',
    totalProfit: 0,
    totalRevenue: 0,
    totalbudget: 0,
    year: '2017',
  };

test(`Highest Revenue By Category`, () => {
  // eslint-disable-next-line no-console
  console.log(`================`);
  // eslint-disable-next-line no-console
  console.log(`{"Revenue": "2787965087",
            "budget": "237000000",
            "category": "Science Fiction",
            "name": "Avatar",
            "profit": 13428184744,
            "share": 18.997095554109247,
            "totalBudget": 7644466762,
            "totalRevenue": 21072651506,
            "year": "2009"}`);
  // eslint-disable-next-line no-console
  console.log(`================`);
  return expect(highestMovieCategorywise().then((movies)=>{
    return movies[0];
  })).resolves.toEqual(output);
});
