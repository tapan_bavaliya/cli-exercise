const totalProfitCategoryWise = require('./totalProfitCategoryWise');

const categoryWiseMoviesWithProfit = (categoryWiseMovieList) =>{
  const profitAndCategoryName = categoryWiseMovieList.map((movie)=>{
    return {
      CategoryName: movie.CategoryName,
      profit: totalProfitCategoryWise(movie.CategoryMovies),
    };
  });
  return profitAndCategoryName;
};
module.exports = categoryWiseMoviesWithProfit;
