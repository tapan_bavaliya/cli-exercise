const fs = require('fs');
const csv = require('fast-csv');
const filePath = './tmdb_5000_movies.csv';

// Using csv parse string to object
module.exports = (csvPath) => {
  // eslint-disable-next-line no-unneeded-ternary
  let path = csvPath ? csvPath : filePath;
  let parsedMovies = [];
  // eslint-disable-next-line no-console
  console.log('... Loading ...');
  return new Promise((resolve) => {
    fs.createReadStream(path)
      .pipe(csv())
      .on('data', (data) => {
        parsedMovies.push({
          Budget: data[0],
          genres: data[1],
          id: data[3],
          original_language: data[5],
          original_title: data[6],
          release_date: data[11],
          revenue: data[12],
          title: data[17],
          vote_average: data[18],
          vote_count: data[19],
        });
      })
      .on('end', () => {
        parsedMovies.shift();
        resolve(parsedMovies);
      });
  }).catch((error) =>{
    // eslint-disable-next-line no-console
    console.log(error);
  });
};
