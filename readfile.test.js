const fileStream = require('./readfile.js');

const filepath = './tmdb_5000_movies.csv';

const movies = {
  Budget: '237000000',
  // eslint-disable-next-line max-len
  genres: `[{"id": 28, "name": "Action"}, {"id": 12, "name": "Adventure"}, {"id": 14, "name": "Fantasy"}, {"id": 878, "name": "Science Fiction"}]`,
  id: '19995',
  original_language: 'en',
  original_title: 'Avatar',
  release_date: '2009-12-10',
  revenue: '2787965087',
  title: 'Avatar',
  vote_average: '7.2',
  vote_count: '11800',
};

test(`Check Parser`, ()=>{
  // eslint-disable-next-line no-console
  console.log(`================`);
  // eslint-disable-next-line no-console
  console.log(`{
  Budget: '237000000',
  genres: [{"id": 28, "name": "Action"}, 
          {"id": 12, "name": "Adventure"}, 
          {"id": 14, "name": "Fantasy"}, 
          {"id": 878, "name": "Science Fiction"}],
  id: '19995',
  original_language: 'en',
  original_title: 'Avatar',
  release_date: '2009-12-10',
  revenue: '2787965087',
  title: 'Avatar',
  vote_average: '7.2',
  vote_count: '11800',
};`);
  // eslint-disable-next-line no-console
  console.log(`================`);
  return expect(fileStream(filepath).then((movie)=>{
    return movie[0];
  })).resolves.toEqual(movies);
});
