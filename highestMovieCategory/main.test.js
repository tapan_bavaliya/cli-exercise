const main = require('./main');

const output = {
  revenues: '99965753',
  categories: 'Action',
  original_titles: 'Hitman',
  release_dates: '2007-11-21',
  budgets: '24000000',
  original_languages: 'en',
  vote_averages: '5.9',
  titles: 'Hitman',
};

test(`Highest Revenue By Category`, () => {
  // eslint-disable-next-line no-console
  console.log(`================`);
  // eslint-disable-next-line no-console
  console.log(`{
    revenues: '99965753',
    categories: 'Action',
    original_titles: 'Hitman',
    release_dates: ['2007', '11', '21'],
    budgets: '24000000',
    original_languages: 'en',
    vote_averages: '5.9',
    titles: 'Hitman',
  };`);
  // eslint-disable-next-line no-console
  console.log(`================`);

  return expect(main().then((movies)=>{
    return movies[0];
  })).resolves.toEqual(output);
});
