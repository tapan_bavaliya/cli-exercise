const main = require('./main');
const Table = require('cli-table');

const table = new Table({
  head: ['categories',
    'title',
    'original_title',
    'release_date',
    'budget',
    'original_language',
    'revenue',
    'vote_average'],
  colWidths: [12, 30, 30, 13, 10, 5, 11, 5],
});
// Print in table view
main().then((movies)=>{
  movies.forEach((movie)=>{
    table.push([
      [movie.categories],
      [movie.titles],
      [movie.original_titles],
      [movie.release_dates],
      [movie.budgets],
      [movie.original_languages],
      [movie.revenues],
      [movie.vote_averages],
    ]);
    return false;
  });
}).then(()=>{
  // eslint-disable-next-line no-console
  console.log(table.toString());
});
