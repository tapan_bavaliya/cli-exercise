const categoryWiseHighest = require('./categoryWiseHighest');

const movielist = (category, parsedMovies) => {
  let moviesThatMatchCategories = [];

  // make a list of Revenue in that all object of array
  parsedMovies.forEach((movie)=>{
    const categories = JSON.parse(movie.genres);
    categories.forEach((movieCategory) =>{
      if (movieCategory.name === category){
        moviesThatMatchCategories.push(movie);
      }
      return false;
    });
    return false;
  });

  return categoryWiseHighest(moviesThatMatchCategories, category);
};
module.exports = movielist;
