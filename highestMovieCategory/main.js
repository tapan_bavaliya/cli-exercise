const readfile = require('../readfile');
const movies = require('./movies');
const findCategoryName = require('../sortedMovieByHighestCategory/findCategoryName');
const removeDuplicateCategory = require('../sortedMovieByHighestCategory/removeDuplicateCategory');

const sortMovieByCategory = () =>{
  return readfile().then((parsedMovies)=>{
    let movieslist = [];

    // List All Categories
    const categoriesList = findCategoryName(parsedMovies);

    // Remove same element in that
    const removeDuplicateInCategoryList = removeDuplicateCategory(categoriesList);

    // Each element in array find highest revenue movie
    removeDuplicateInCategoryList.forEach((category)=>{
      movieslist.push(movies(category, parsedMovies));
    });

    // sort it by category
    const sortedMovieByRevenue = movieslist.sort((first, second)=>{
      return second.revenues - first.revenues;
    });

    return sortedMovieByRevenue;
  });
};
module.exports = sortMovieByCategory;
