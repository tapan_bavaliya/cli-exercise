const categoryWiseHighestMovie = (moviesThatMatchCategories, category)=>{
  let highestRevenue = 0;
  let moviesObject;

  // Find that object ewhich has highest revenue
  moviesThatMatchCategories.forEach((movie)=>{
    if (highestRevenue <= movie.revenue){
      highestRevenue = movie.revenue;
      moviesObject = {
        revenues: movie.revenue,
        categories: category,
        original_titles: movie.original_title,
        release_dates: movie.release_date,
        budgets: movie.Budget,
        original_languages: movie.original_language,
        vote_averages: movie.vote_average,
        titles: movie.title,
      };
    }
    return false;
  });
  return moviesObject;
};
module.exports = categoryWiseHighestMovie;
