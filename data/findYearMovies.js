const movies = [
  {
    Budget: '237000000',
    // eslint-disable-next-line max-len
    genres: '[{"id": 28, "name": "Action"}, {"id": 12, "name": "Adventure"}, {"id": 14, "name": "Fantasy"}, {"id": 878, "name": "Science Fiction"}]',
    id: '19995',
    original_language: 'en',
    original_title: 'Avatar',
    release_date: '2009-12-10',
    revenue: '2787965087',
    title: 'Avatar',
    vote_average: '7.2',
    vote_count: '11800',
  },
  {
    Budget: '300000000',
    // eslint-disable-next-line max-len
    genres: '[{"id": 12, "name": "Adventure"}, {"id": 14, "name": "Fantasy"}, {"id": 28, "name": "Action"}]',
    id: '285',
    original_language: 'en',
    original_title: 'Pirates of the Caribbean: At World\'s End',
    release_date: '2007-05-19',
    revenue: '961000000',
    title: 'Pirates of the Caribbean: At World\'s End',
    vote_average: '6.9',
    vote_count: '4500',
  },
];
module.exports = movies;
