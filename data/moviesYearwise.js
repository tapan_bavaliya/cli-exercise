const moviesYearWise =
  [
    {
      Budget: '237000000',
      genres: `
    [
        {"id": 28, "name": "Action"},
        {"id": 12, "name": "Adventure"},
        {"id": 14, "name": "Fantasy"},
        {"id": 878, "name": "Science Fiction"}
    ]`,
      id: '19995',
      original_language: 'en',
      original_title: 'Avatar',
      release_date: '2009-12-10',
      revenue: '2787965087',
      title: 'Avatar',
      vote_average: '7.2',
      vote_count: '11800',
    },
    {
      Budget: '250000000',
      genres: `
    [
        {"id": 12, "name": "Adventure"},
        {"id": 14, "name": "Fantasy"},
        {"id": 10751, "name": "Family"}
    ]`,
      id: '767',
      original_language: 'en',
      original_title: 'Harry Potter and the Half-Blood Prince',
      release_date: '2009-07-07',
      revenue: '933959197',
      title: 'Harry Potter and the Half-Blood Prince',
      vote_average: '7.4',
      vote_count: '5293',
    },
  ];
module.exports = moviesYearWise;
