const highestMovieObject = `
{ 
  year: '2009',
  totalBudget: 7644466762,
  totalRevenue: 21072651506,
  profit: 13428184744,
  category: 'Action',
  name: 'Avatar',
  budget: '237000000',
  Revenue: '2787965087',
  share: 18.997095554109247 
}`;
module.exports = highestMovieObject;
